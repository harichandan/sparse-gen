# Sparse File Generator 
[![Gem Version](https://badge.fury.io/rb/sparse-gen.svg)](http://badge.fury.io/rb/sparse-gen)

A gem to create sparse files given the percentage of sparseness

## Usage
#####Single Sparse File

	SparseGen.sparse_gen_percent(filename, percent, num_blocks, block_size)

where 

	filename   = path of file to be created
   	percent    = percentage of sparseness (default 50)
   	num_blocks = number of blocks (default 1024*1024)
   	block_size = size of each block (default 1024)

#####Directory with sparse files

	SparseGen.sparse_dir(dirname, num_files, percent, num_blocks, block_size)

where

	dirname    = path of the directory to be created
	num_files  = number of files in the directory (default 1000)
	percent    = percentage of sparseness (default 50)
	num_blocks = number of blocks (default 1024)
	block_size = size of each block (default 1024)
