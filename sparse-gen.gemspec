version = File.read('VERSION').strip

Gem::Specification.new do |s|
  s.name        = 'sparse-gen'
  s.version     = version
  s.licenses    = ['Apache-2.0']
  s.date        = '2015-10-01'
  s.summary     = 'Sparse File Generator'
  s.description = 'Generate sparse files given the percentage of sparseness. For usage, check documentation'
  s.authors     = ['Harichandan Pulagam']
  s.email       = 'pharic.15@gmail.com'
  s.files       = Dir['lib/**/*.rb']
end
